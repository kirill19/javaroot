package omsu.javaprojects.trinomial;

public class TrinomialRooter {
    private QuadTrinomial qt;

    public TrinomialRooter(QuadTrinomial qt) {
        this.qt = qt;
    }

    public double getMaxRoot() {
        if (qt.getRoots() == null) {
            throw new RuntimeException();
        } else {
            return (qt.getRoots()[0] > qt.getRoots()[1]) ? qt.getRoots()[0] : qt.getRoots()[1];
        }
    }
}
